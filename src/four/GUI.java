package four;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;


public class GUI extends JFrame{
	private static final int FRAME_WIDTH = 500;
	private static final int FRAME_HEIGHT = 400;
	private JPanel colorPanel, buttonPanel, mainPanel;
	private JComboBox<String> comboBox;
	
	public GUI(){
		createFrame();
		
		comboBox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) { 
				getSelected();
			}
		});
		
	}
	
	public void createFrame(){
		setSize(FRAME_WIDTH, FRAME_HEIGHT);
		
		mainPanel = new JPanel();
		mainPanel.setLayout(new BorderLayout());
		colorPanel = new JPanel();
		buttonPanel = new JPanel();
		buttonPanel.setLayout(new FlowLayout());
		
		comboBox = new JComboBox<String>();
		comboBox.addItem("Red");
		comboBox.addItem("Green");
		comboBox.addItem("Blue");
		
		colorPanel.setBackground(Color.BLACK);
		buttonPanel.add(comboBox);
		
		mainPanel.add(colorPanel,BorderLayout.CENTER);
		mainPanel.add(buttonPanel,BorderLayout.SOUTH);
		
		add(mainPanel);

	}

	public void getSelected(){
		int index = comboBox.getSelectedIndex();
		if(index == 0){
			colorPanel.setBackground(Color.RED);
		}
		else if(index ==1){
			colorPanel.setBackground(Color.GREEN);
		}
		else if(index ==2){
			colorPanel.setBackground(Color.BLUE);
		}
	}
	
}
