package five;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;


public class GUI extends JFrame{
	private static final int FRAME_WIDTH = 500;
	private static final int FRAME_HEIGHT = 400;
	private JPanel colorPanel, buttonPanel, mainPanel;
	private JMenuBar menuBar;
	private JMenu menu;
	private JMenuItem red, green, blue;
	
	public GUI(){
		createFrame();
		
		red.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) { 
				setRed();
			}
		});
		
		green.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) { 
				setGreen();
			}
		});
		
		blue.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) { 
				setBlue();
			}
		});
	}
	
	public void createFrame(){
		setSize(FRAME_WIDTH, FRAME_HEIGHT);
		
		mainPanel = new JPanel();
		mainPanel.setLayout(new BorderLayout());
		colorPanel = new JPanel();
		buttonPanel = new JPanel();
		buttonPanel.setLayout(new FlowLayout());
		
		menu = new JMenu("Color");
		red = new JMenuItem("Red");
		green = new JMenuItem("Green"); 
		blue = new JMenuItem("Blue");
		menu.add(red);
		menu.add(green);
		menu.add(blue);
		
		menuBar = new JMenuBar();
		menuBar.add(menu);
		
		colorPanel.setBackground(Color.BLACK);
		buttonPanel.add(menuBar);
		
		mainPanel.add(colorPanel,BorderLayout.CENTER);
		mainPanel.add(buttonPanel,BorderLayout.SOUTH);
		
		add(mainPanel);

	}
	
	public void setRed(){
		colorPanel.setBackground(Color.RED);
	}
	public void setGreen(){
		colorPanel.setBackground(Color.GREEN);
	}
	public void setBlue(){
		colorPanel.setBackground(Color.BLUE);
	}
	
}
