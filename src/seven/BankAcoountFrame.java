package seven;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class BankAcoountFrame extends JFrame{
	private JPanel panel1,panel2;
	private JLabel addLabel;
	private JButton addButton, outButton;
	private JTextField addField;
	private JTextArea resultArea;
	private BankAccount bank;
	
	public BankAcoountFrame(){
		bank = new BankAccount();
		resultArea = new JTextArea(10,30);
		createTextField();
		createButton();
		createPanel();
		setSize(500, 300);
	}
	
	public void createTextField(){
		addLabel = new JLabel("Total:");
		addField = new JTextField(10);
		addField.setText("1000");
	}
	
	public void createButton(){
		addButton = new JButton("deposit");
		outButton = new JButton("withdraw");
		
		addButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) { 
				double amount = Double.parseDouble(addField.getText());
				bank.deposit(amount);
				resultArea.append("deposit: "+amount+"  Balance: "+bank.getBalance()+"\n");
			}
		});
		
		outButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) { 
				double amount = Double.parseDouble(addField.getText());
				bank.withdraw(amount);
				resultArea.append("withdraw: "+amount+"  Balance: "+bank.getBalance()+"\n");
			}
		});
		
	}
	public void createPanel(){
		panel1 = new JPanel();
		panel1.add(addLabel);
		panel1.add(addField);
		panel1.add(addButton);
		panel1.add(outButton);
		
		panel2 = new JPanel();
		JScrollPane scroll = new JScrollPane(resultArea);
		panel2.add(scroll);
		
				
		add(panel1,BorderLayout.NORTH);
		add(panel2,BorderLayout.CENTER);
	}

}
