package three;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;


public class GUI extends JFrame{
	private static final int FRAME_WIDTH = 500;
	private static final int FRAME_HEIGHT = 400;
	private JPanel colorPanel, buttonPanel, mainPanel;
	private JCheckBox red, green, blue;
	
	public GUI(){
		createFrame();
		
		red.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) { 
				getSelected();
			}
		});
		
		green.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) { 
				getSelected();
			}
		});
		
		blue.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) { 
				getSelected();
			}
		});
	}
	
	public void createFrame(){
		setSize(FRAME_WIDTH, FRAME_HEIGHT);
		
		mainPanel = new JPanel();
		mainPanel.setLayout(new BorderLayout());
		colorPanel = new JPanel();
		buttonPanel = new JPanel();
		buttonPanel.setLayout(new FlowLayout());
		
		red = new JCheckBox("Red");
		green = new JCheckBox("Green"); 
		blue = new JCheckBox("Blue");
		
		colorPanel.setBackground(Color.BLACK);
		buttonPanel.add(red);
		buttonPanel.add(green);
		buttonPanel.add(blue);
		
		mainPanel.add(colorPanel,BorderLayout.CENTER);
		mainPanel.add(buttonPanel,BorderLayout.SOUTH);
		
		add(mainPanel);

	}
	
	public void setRed(){
		
	}
	public void setGreen(){
		colorPanel.setBackground(Color.GREEN);
	}
	public void setBlue(){
		colorPanel.setBackground(Color.BLUE);
	}
	
	public void getSelected(){
		if(red.isSelected()){
			colorPanel.setBackground(Color.RED);
		}
		if(green.isSelected()){
			colorPanel.setBackground(Color.GREEN);
		}
		if(blue.isSelected()){
			colorPanel.setBackground(Color.BLUE);
		}
		if(red.isSelected() && green.isSelected()){
			colorPanel.setBackground(Color.YELLOW);
		}
		if(red.isSelected() && blue.isSelected()){
			colorPanel.setBackground(Color.MAGENTA);
		}
		if(green.isSelected() && blue.isSelected()){
			colorPanel.setBackground(Color.CYAN);
		}
		if(red.isSelected() && green.isSelected() && blue.isSelected()){
			colorPanel.setBackground(Color.WHITE);
		}
		if (!red.isSelected() && !green.isSelected() && !blue.isSelected()){
			colorPanel.setBackground(Color.BLACK);
		}
		
	}
}
