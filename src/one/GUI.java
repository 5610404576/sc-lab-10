package one;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;


public class GUI extends JFrame{
	private static final int FRAME_WIDTH = 500;
	private static final int FRAME_HEIGHT = 400;
	private JPanel colorPanel, buttonPanel, mainPanel;
	private JButton red, green, blue;
	
	public GUI(){
		createFrame();
		
		red.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) { 
				setRed();
			}
		});
		
		green.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) { 
				setGreen();
			}
		});
		
		blue.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) { 
				setBlue();
			}
		});
	}
	
	public void createFrame(){
		setSize(FRAME_WIDTH, FRAME_HEIGHT);
		
		mainPanel = new JPanel();
		mainPanel.setLayout(new BorderLayout());
		colorPanel = new JPanel();
		buttonPanel = new JPanel();
		buttonPanel.setLayout(new FlowLayout());
		
		red = new JButton("Red");
		green = new JButton("Green"); 
		blue = new JButton("Blue");
		
		colorPanel.setBackground(Color.BLACK);
		buttonPanel.add(red);
		buttonPanel.add(green);
		buttonPanel.add(blue);
		
		mainPanel.add(colorPanel,BorderLayout.CENTER);
		mainPanel.add(buttonPanel,BorderLayout.SOUTH);
		
		add(mainPanel);

	}
	
	public void setRed(){
		colorPanel.setBackground(Color.RED);
	}
	public void setGreen(){
		colorPanel.setBackground(Color.GREEN);
	}
	public void setBlue(){
		colorPanel.setBackground(Color.BLUE);
	}
	
}
