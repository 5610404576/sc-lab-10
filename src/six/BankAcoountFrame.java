package six;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class BankAcoountFrame extends JFrame{
	private JPanel panel1,panel2;
	private JLabel addLabel,resultLabel;
	private JButton addButton, outButton;
	private JTextField addField;
	private BankAccount bank;
	
	public BankAcoountFrame(){
		bank = new BankAccount();
		resultLabel = new JLabel("Balance: "+bank.getBalance());
		createTextField();
		createButton();
		createPanel();
		setSize(400, 150);
	}
	
	public void createTextField(){
		addLabel = new JLabel("Total:");
		addField = new JTextField(10);

	}
	
	public void createButton(){
		addButton = new JButton("deposit");
		outButton = new JButton("withdraw");
		
		addButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) { 
				double amount = Double.parseDouble(addField.getText());
				bank.deposit(amount);
				resultLabel.setText("Balance: "+bank.getBalance());
			}
		});
		
		outButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) { 
				double amount = Double.parseDouble(addField.getText());
				bank.withdraw(amount);
				resultLabel.setText("Balance: "+bank.getBalance());
			}
		});
		
	}
	public void createPanel(){
		panel1 = new JPanel();
		panel1.add(addLabel);
		panel1.add(addField);
		panel1.add(resultLabel);
	
		
		panel2 = new JPanel();
		panel2.add(addButton);
		panel2.add(outButton);
				
		add(panel1,BorderLayout.NORTH);
		add(panel2,BorderLayout.CENTER);
		
	}
}
